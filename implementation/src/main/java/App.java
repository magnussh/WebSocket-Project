import com.google.gson.Gson;
import server.api.Connection;
import server.api.Server;

import java.io.IOException;

/*
 *
 */
public class App {
    public static void main(String[] args) {
        try {
            Server server = Server.getWebSocketServer(8080);
            Gson gson = new Gson();

            server.onConnection((Connection connection) -> {
                System.out.println("Client Connected");

                connection.onMessage(message -> {
                    System.out.println("Message recieved: " + message);
                    connection.send(gson.toJson(new Message("Hello there")));
                });
                server.sendAll(gson.toJson(new Message("Send to everyone!")));

            connection.onError((ex) -> {
                System.out.println("An error occured");
            });

            connection.onClose(() -> {
                System.out.println(connection + ": Client disconnected");
            });

        });

        server.onError((ex) -> {

            System.out.println("Exception happened!");
            System.out.println(ex);

            server.close();

        });

            System.out.println("Server listening");
            server.listen();
            System.out.println("Listen returned");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

class Message {
    private final String message;

    Message(String message) {
        this.message = message;
    }
}