package WebChat;

import server.api.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * example Chat server
 */
public class ChatApp {

    public static void main(String[] args) throws IOException {

        List<String> messages = new ArrayList<>();

        Server server = Server.getWebSocketServer(8080);

        server.onConnection(connection -> {

            messages.forEach(connection::send);  // Send all the messages to the new Connection

            connection.onMessage(s -> {

                System.out.println("Message recieved: "+s);

                messages.add(s);
                server.sendAll(s); // Send the new message to all Connections

            });

            connection.onClose(() -> {

                System.out.println(connection+" disconnected!");
            });

            connection.onError(throwable -> {
                System.out.println("An error occured: " + throwable);
            });

        });

        server.onError(e -> {

            System.out.println("Something went wrong!" +e);

        });

        System.out.println("Server listening");
        server.listen();
    }
}