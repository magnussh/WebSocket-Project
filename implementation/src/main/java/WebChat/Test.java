package WebChat;

import server.api.Server;

import java.io.IOException;

/**
 * Created by faiter on 4/26/17.
 */
public class Test {

    public static void main(String[] args) throws IOException {

        Server webSocketServer = Server.getWebSocketServer(8080);

        webSocketServer.onConnection(connection -> {


            System.out.println("Someone connected");


            connection.onMessage(s -> {

                connection.send(s);
            });

        });

        webSocketServer.listen();
    }
}
