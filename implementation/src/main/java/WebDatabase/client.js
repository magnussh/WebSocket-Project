//When this file is fully loaded, initialize board with context
$(document).ready(function(){

    var ws = new WebSocket("ws://10.20.195.88:8080"); // hvis du bruke localhost funke det ikke på andre maskina

    ws.onerror = (error) => {
        $("#connection_label").html("Not connected");
    };
    ws.onopen = () => {
        $("#connection_label").html("Connected");
    };

    ws.onmessage = (event) => {

        console.log(event);
        console.log(event.data);

        var data = JSON.parse(event.data);

        console.log(data.key);

        $('#table tr:last')
            .after(
                "<tr>"+
                "<td>"+data.key+"</td>"+
                "<td>"+data.value+"</td>"+
                "</tr>"
              );

    };

    $("#sendButton").click(function() {

        var key = $("#key").val();
        var value = $("#value").val();

        ws.send(JSON.stringify({key, value}));

        $("#key").val("");
        $("#value").val("");
    });
});
