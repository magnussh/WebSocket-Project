package WebDatabase;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import server.api.Server;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by OlavH on 4/26/17.
 */
public class WebDatabase {

    static Map<String, String> database = new HashMap<>();

    static Gson gson = new Gson();

    static{

        database.put("AEGIS","All-wavelength Extended Groth strip International Survey");
        database.put("LEO","Low Earth Orbit");
        database.put("AGU","Advanced Grabbing Unit");
        database.put("Ap","Apoapsis, highest point in an orbit.");


    }

    public static void main(String[] args) throws IOException {

        Server ws = Server.getWebSocketServer(8080);

        ws.onConnection(connection -> {

            database.entrySet().forEach(stringStringEntry -> {
                connection.send(gson.toJson(stringStringEntry));
            });


            connection.onMessage(message ->{

                Type type = new TypeToken<HashMap<String,String>>(){}.getType();

                HashMap<String, String> map = gson.fromJson(message, type);

                map.forEach((key, value) -> database.put(key, value));

                ws.getClients().forEach(connection1 -> {

                    connection1.send(gson.toJson(map));

                });
            });
        });

        System.out.println("Listening");
        ws.listen();
    }
}
