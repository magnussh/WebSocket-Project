SURF WebSocket
=======

[ ![Download](https://api.bintray.com/packages/olavh/surf-websocket/server/images/download.svg) ](https://bintray.com/olavh/surf-websocket/server/_latestVersion)

SURF (Simply Useable Reactive Functional) WebSocket is a framework for server-side programming of a WebSocket-server.

# Gradle
To import SURF-WebSocket from gradle add this to your repositories.

```
maven {
        url  "http://dl.bintray.com/olavh/surf-websocket"
}
```

And add this to gradle dependencies.

```
compile 'WebSocket-Project:api:0.1.0'
```

# Use of server

To start the server use the static method from the server interface to get a server object with your port specified in the arguments.

```java
Server server = Server.getWebSocketServer(8080);
```

In this chat example we add messages recieved from clients to a List of Strings.

```java
List<String> messages = new ArrayList<>();
```

Further on you need to define what the server will do when it recieves a connection from a client.

```java
server.onConnection(connection -> {

    messages.forEach(connection::send);  // Send all the messages to the new Connection

    connection.onMessage(s -> {

        System.out.println("Message recieved: "+s);

        messages.add(s);
        server.sendAll(s); // Send the new message to all Connections

    });

    connection.onClose(() -> {

        System.out.println(connection+" disconnected!");
    });

    connection.onError(throwable -> {
        System.out.println("An error occured: " + throwable);
    });

});
```

It is recommended to define an onError- and onClose-method, for logging and handling of eventual errors and closing of connections.

It is also recommended to define an onError-method for the server, in case of a server crash.

```java
server.onError(e -> {

    System.out.println("Something went wrong!" +e);

});
```

To start the server for listening simply use the listen-method.

```java
server.listen();
```

# Features
- Multi client communication
- ReactiveX and functional
- String and byte messages
- Frame fragments
    - Continuation frames
- Control frames
    - Ping/pong
    - Close
- 31-bit payload support
- Multi threading
- Some WebSocket Protocol violation detection

# Sources
https://tools.ietf.org/html/rfc6455

https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers

http://reactivex.io/