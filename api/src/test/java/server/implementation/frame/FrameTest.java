package server.implementation.frame;

import org.junit.Test;
import org.testng.Assert;

import java.nio.ByteBuffer;
import java.util.Random;
import static org.junit.Assert.*;

/**
 * WebSocket-Project
 * 01.05.2017
 */
public class FrameTest {
    @Test
    public void fromFrameBytes() throws Exception {
        Random rng = new Random();
        int mask = rng.nextInt();

        byte[] maskArray = ByteBuffer.allocate(4).putInt(mask).array();

        String msg = "abcd";

        byte[] msgBytes = msg.getBytes();
        byte[] masked = new byte[msgBytes.length];

        for (int i = 0; i < masked.length; i++) {
            masked[i] = (byte) (msgBytes[i] ^ maskArray[i % 4]);
        }

        ByteBuffer msgBuffer = ByteBuffer.allocate(6 + masked.length)
                .put((byte) 0b10000001)
                .put((byte) 0b10000100)
                .put(maskArray)
                .put(masked);


        Frame frame = Frame.fromFrameBytes(msgBuffer.array());

        assertArrayEquals("Mask",frame.mask, maskArray);

        assertEquals("Data",FrameDecoder.unmaskText(frame),msg);

    }

    @Test
    public void toBytes() throws Exception {


    }

}