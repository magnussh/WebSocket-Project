package server.implementation.frame;

import server.api.WebSocketProtocolViolation;
import server.implementation.util.Opcode;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static server.implementation.util.Opcode.*;

/**
 * Class representing a WebSocket Frame
 * <p>
 * Includes methods for transforming to and from socket bytes
 */
public class Frame {
    public final boolean FIN;
    public final Opcode opcode;
    public final int payloadLength;
    public final byte payloadStart;
    public final byte[] data; // Data is entire frame when receiving, and only payload data when sending.
    public byte[] mask;

    private Frame(boolean fin, Opcode opcode, int payloadLength, byte payloadStart, byte[] data) {
        FIN = fin;
        this.opcode = opcode;
        this.payloadLength = payloadLength;
        this.payloadStart = payloadStart;
        this.data = data;
    }

    public static Frame fromFrameBytes(byte[] frame) {
        boolean FIN = frame[0] < 0;
        boolean masked = frame[1] < 0;

        Opcode opcode = getOpcode(frame[0]);

        boolean dataFrame = opcode == TEXT_FRAME || opcode == BINARY_FRAME;

        if (!masked && dataFrame) {
            throw new WebSocketProtocolViolation("Client sent unmasked Data Frame [Section 5.3]");
        }

        int loc = 1;
        int length = frame[loc] & 0b01111111; // fjerner første bit
        long length64;

        if (length <= 0) {
            length = 0;
            return new Frame(FIN, opcode, length, findPayloadStart(length, false), frame);
        } else if (length == 126) {
            length = (frame[++loc] & 0xFF) << 8 | frame[++loc] & 0xFF;
        } else if (length == 127) {
            // 64-bit payload length
            length64 = ((long) frame[++loc] & 0xFF) << 56 |
                    ((long) frame[++loc] & 0xFF) << 48 |
                    ((long) frame[++loc] & 0xFF) << 40 |
                    ((long) frame[++loc] & 0xFF) << 32 |
                    ((long) frame[++loc] & 0xFF) << 24 |
                    ((long) frame[++loc] & 0xFF) << 16 |
                    ((long) frame[++loc] & 0xFF) << 8 |
                    ((long) frame[++loc] & 0xFF);

            if (length64 / Integer.MAX_VALUE > 0) {
                // close connection with status code 1009 (A message that is too big for it to process)
                // to be removed in future update with 64-bit payload support
                throw new WebSocketProtocolViolation("1009");
            } else if (length64 < 0) {
                // Protocol error, most significant bit can't be 1
                throw new WebSocketProtocolViolation("1002");
            }

            // because Java Arrays are indexed by signed integers, and above else if filters out lengths over 31-bits
            length = (int) length64;
        }

        byte payloadStart = findPayloadStart(length, true);
        byte[] mask = Arrays.copyOfRange(frame, payloadStart - 4, payloadStart);
        return new Frame(FIN, opcode, length, payloadStart, frame).setMask(mask);
    }

    /**
     * @param opcode Type of control frame to create
     * @return Control Frame
     * @throws IllegalArgumentException if requested opcode is not a control frame type
     */
    public static Frame createFrame(Opcode opcode) {
        return createFrame(opcode, new byte[0]);
    }

    /**
     * @param opcode Type of control frame to create
     * @param data   Payload Data
     * @return Control Frame
     * @throws IllegalArgumentException if requested opcode is not a control frame type
     */
    public static Frame createFrame(Opcode opcode, byte[] data) {
        return new Frame(true, opcode, data.length, findPayloadStart(data.length, false), data);
    }

    private static byte findPayloadStart(int length, boolean masked) {
        byte start = (byte) (masked ? 4 : 0);

        if (length <= 125) {
            start += 2;
        } else if (length <= 65535) {
            start += 4;
        } else {
            start += 10;
        }

        return start;
    }

    private Frame setMask(byte[] mask) {
        this.mask = mask;
        return this;
    }

    /**
     * Prepares Frame for sending
     *
     * @return Coded WebSocket frame, ready for sending to client.
     */
    public byte[] toBytes() {
        int totalLength = payloadStart + payloadLength;
        byte[] frameBytes = new byte[totalLength];
        if (FIN) {
            frameBytes[0] = (byte) (opcode.getValue() - 128);
        } else {
            frameBytes[0] = (byte) opcode.getValue();
        }

        if (data.length == 0) {
            frameBytes[1] = 0;
            return frameBytes;
        }
        int length = data.length;
        if (length <= 125) {
            frameBytes[1] = (byte) length;
        } else if (length <= 65535) {
            frameBytes[1] = 126;
            frameBytes[2] = (byte) (length >>> 8);
            frameBytes[3] = (byte) length;
        } else if (length <= Integer.MAX_VALUE - 10) {
            frameBytes[1] = 127;
            byte[] len = ByteBuffer.allocate(Integer.BYTES).putInt(length).array();
            for (int i = len.length - 1; i >= 0; i--) {
                frameBytes[payloadStart - (len.length - i)] = len[i];
            }
        } else {
            throw new IllegalArgumentException("Can't encode data of length " + (Integer.MAX_VALUE - 10) + " or longer. Arrays of that size are not supported in Java");
        }

        System.arraycopy(data, 0, frameBytes, payloadStart, payloadLength);
        return frameBytes;
    }
}
