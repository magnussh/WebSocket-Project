package server.implementation.frame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.Arrays;

import static server.implementation.util.Opcode.CONTINUATION_FRAME;
import static server.implementation.util.Opcode.TEXT_FRAME;

/**
 * This class is responsible for unmasking data received from clients
 */
public class FrameDecoder {
    private static final Logger logger = LoggerFactory.getLogger(FrameDecoder.class);

    public static String unmaskText(Frame dataFrame) {
        if(dataFrame.opcode != TEXT_FRAME &&dataFrame.opcode != CONTINUATION_FRAME) throw new IllegalArgumentException("unmaskText only supports text frames");

        byte[] decoded = unmaskBinary(dataFrame);
        String s = new String(decoded, Charset.forName("UTF-8"));
        logger.debug("Decoded message: {}", s);
        return s;
    }

    public static byte[] unmaskBinary(Frame dataFrame) {
        logger.debug("DECODING");

        int length = dataFrame.payloadLength;
        int payloadStart = dataFrame.payloadStart;
        byte[] decoded = new byte[length];
        byte[] encoded = dataFrame.data;
        byte[] mask = dataFrame.mask;

        logger.debug("Mask: {}", Arrays.toString(dataFrame.mask));

        for (int i = 0; i < length; i++) {
            decoded[i] = (byte) (encoded[payloadStart + i] ^ mask[i & 0x3]);
        }
        return decoded;
    }
}

