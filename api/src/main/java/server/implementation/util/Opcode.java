package server.implementation.util;

/**
 * Enum representing WebSocket opcodes
 */
public enum Opcode {

    CONTINUATION_FRAME(0),
    TEXT_FRAME(1),
    BINARY_FRAME(2),
    CONNECTION_CLOSE(8),
    PING(9),
    PONG(0xA);

    private int value;

    Opcode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Opcode valueOf(int opcode) {

        for (Opcode code : values()){
            if (code.getValue() == opcode) return code;
        }

        throw new IllegalArgumentException("Invalid opcode");

    }

    public static Opcode getOpcode(byte b) {
        byte mask = 0b00001111;
        int masked = b & mask;

        return Opcode.valueOf(masked);
    }
}