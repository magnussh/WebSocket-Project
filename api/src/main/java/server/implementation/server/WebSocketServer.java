package server.implementation.server;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.api.Connection;
import server.api.Server;
import server.implementation.connection.Client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Java WebSocket server API
 */
public class WebSocketServer implements Server {
    private final String secWebSocketKey = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

    private static final Logger logger = LoggerFactory.getLogger(WebSocketServer.class);
    private final ServerSocket server;

    private ReplaySubject<Connection> connectionSubject;
    private PublishSubject<Exception> exceptionSubject;

    public WebSocketServer(int port) throws IOException {
        server = new ServerSocket(port);
        server.setSoTimeout(1000);
        connectionSubject = ReplaySubject.create();
        exceptionSubject = PublishSubject.create();
        // TODO: 26.04.2017 Make event loop
        RxJavaPlugins.setFailOnNonBlockingScheduler(true);
    }

    private boolean doHandshake(InputStream in, OutputStream out) {
        String input = new Scanner(in, "UTF-8").useDelimiter("\\r\\n\\r\\n").next();
        String responseString = "HTTP/1.1 101 Switching Protocols\r\n" + "Connection: Upgrade\r\n" + "Upgrade: websocket\r\n" + "Sec-WebSocket-Accept: ";
        Matcher get = Pattern.compile("^GET").matcher(input);

        if (get.find()) {
            Matcher match = Pattern.compile("Sec-WebSocket-Key: (.*)").matcher(input);
            if (match.find()) {
                try {
                    responseString += Base64
                            .getEncoder()
                            .encodeToString(
                                    MessageDigest
                                            .getInstance("SHA-1")
                                            .digest((match.group(1) + secWebSocketKey)
                                                    .getBytes("UTF-8"))
                            )
                            + "\r\n\r\n";
                    byte[] response = responseString.getBytes("UTF-8");
                    out.write(response, 0, response.length);
                    return true;
                } catch (IOException | NoSuchAlgorithmException e1) {
                    logger.warn("Error while on handshake: {}", e1);
                    exceptionSubject.onNext(e1);
                }
            }
        }
        return false;
    }

    @Override
    public void onConnection(Consumer<Connection> connectionAction) {
        connectionSubject.subscribe(connectionAction);
    }

    @Override
    public void listen() {
        Observable.create((ObservableEmitter<Socket> s) -> {
                    while (!server.isClosed()) {
                        try {
                            Socket socket = server.accept();
                            logger.trace("New socket connection established");
                            s.onNext(socket);
                        } catch (SocketTimeoutException ignored) {
                        } catch (Exception e) {
                            exceptionSubject.onNext(e);
                            s.onError(e);
                        }
                    }
                    s.onComplete();
                }
        ).observeOn(Schedulers.single())
                .subscribe(socket -> {

                    InputStream inputStream = socket.getInputStream();
                    OutputStream outputStream = socket.getOutputStream();

                    if (doHandshake(inputStream, outputStream)) {
                        logger.info("Client connected");
                        Client client = new Client(socket);

                        connectionList.add(client);

                        connectionSubject.onNext(client);
                    } else {

                        logger.debug("Handshake failed");
                    }
                }, e -> logger.error("Server Error: {}", e), () -> {
                    logger.info("Closing server");
                    server.close();
                });
    }

    @Override
    public void onError(Consumer<Exception> exceptionAction) {
        exceptionSubject.subscribe(exceptionAction);

    }

    @Override
    public void close() {
        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final List<Connection> connectionList = Collections.synchronizedList(new ArrayList<>());

    @Override
    public synchronized List<Connection> getClients() {

        connectionList.removeIf(connection -> !connection.isConnected());

        return connectionList;
    }

    @Override
    public void sendAll(String string) {
        synchronized (connectionList) {
            connectionList.forEach(connection -> {
                if (connection.isConnected()) {
                    connection.send(string);
                }
            });
        }
    }
}