package server.implementation.connection;

import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.api.Connection;
import server.api.WebSocketProtocolViolation;
import server.implementation.util.Opcode;
import server.implementation.frame.Frame;
import server.implementation.frame.FrameDecoder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Class representing a WebSocket connection to a client
 */
public class Client implements Connection {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);
    private final Socket socket;
    private final int BUFFER_SIZE = 8192;
    private boolean isConnected; // TODO: 27.04.2017 Make code less reliant on this bool. Calling onError or onCompleted more than once causes UndeliverableException
        private boolean pingSent, pongSent, pongRec;
        private final OutputStream outputStream;

    private FlowableProcessor<String> clientProcessor;

    public Client(Socket socket) throws IOException {
        this.socket = socket;
        isConnected = true;
        pingSent = false;
        pongSent = false;
        pongRec = false;

        InputStream inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();

        clientProcessor = PublishProcessor.create();

        Flowable<byte[]> flowable = Flowable.generate(emitter -> {
            // TODO: 27.04.2017 Move some of the logic here?? closing connection etc
            byte[] buffer = new byte[BUFFER_SIZE];
            int count = inputStream.read(buffer);
            if (count == -1) {
                emitter.onComplete();
            } else if (count < BUFFER_SIZE) {
                emitter.onNext(Arrays.copyOf(buffer, count));
            } else {
                emitter.onNext(buffer);
            }
        });

        StringBuilder continuationFrame = new StringBuilder();

        flowable.subscribeOn(Schedulers.newThread()) // TODO: 26.04.2017 Use different scheduler?

                .map(Frame::fromFrameBytes)
                .subscribe(frame -> {

                            switch (frame.opcode) {

                                case CONTINUATION_FRAME:

                                    continuationFrame.append(FrameDecoder.unmaskText(frame));
                                    if (frame.FIN) {
                                        clientProcessor.onNext(continuationFrame.toString());
                                        continuationFrame.delete(0, continuationFrame.length()); // empties the builder
                                    }
                                    break;

                                case TEXT_FRAME:

                                    if (!frame.FIN) {
                                        continuationFrame.append(FrameDecoder.unmaskText(frame));
                                    }
                                    else
                                        clientProcessor.onNext(FrameDecoder.unmaskText(frame));
                                    break;

                                case BINARY_FRAME:
                                    byte[] bytes = FrameDecoder.unmaskBinary(frame);

                                    StringBuilder builder = new StringBuilder();

                                    for (byte aByte : bytes) {
                                        builder.append(Integer.toBinaryString(aByte));
                                    }

                                    clientProcessor.onNext(builder.toString());
                                    break;
                                case CONNECTION_CLOSE:
                                    int code = ((frame.data[frame.payloadStart] & 0xff) << 8) | (frame.data[frame.payloadStart + 1] & 0xff);
                                    closeConnection(code);
                                    clientProcessor.onComplete();
                                    if (closeAction != null) closeAction.run(); // FIXME: 4/27/17
                                    break;
                                case PING:
                                    if(!pongSent) {
                                        byte[] pingBytes = FrameDecoder.unmaskBinary(frame);
                                        sendPong(pingBytes);
                                    }
                                    break;
                                case PONG:
                                    if(pingSent) {
                                        pongRec = true;
                                    }
                                    pingSent = false;
                                    break;
                            }
                        },
                        this::handleError,
                        () -> closeConnection(1008));
    }

    private void handleError(Throwable throwable) {
        // https://tools.ietf.org/html/rfc6455#section-7.4.1
        int status;

        if (throwable instanceof WebSocketProtocolViolation) {
            status = Integer.parseInt(throwable.getMessage());
        } else {
            status = 1011;
        }

        if (isConnected) {
            clientProcessor.onError(throwable);
            closeConnection(status);
        }
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }


    private void closeConnection(int statusCode) {
        if (!isConnected) return;
        clientProcessor.onComplete();

        short status = (short) statusCode;
        byte[] data = new byte[2];
        data[0] = (byte) (status >>> 8);
        data[1] = (byte) (status & 0xFF);
        Frame frame = Frame.createFrame(Opcode.CONNECTION_CLOSE, data);

        // TODO: 26.04.2017 wait for response close frame from client
        try {
            send(frame);
            socket.close();
            isConnected = false;
        } catch (IOException e) {
            logger.error("Error closing connection to client: {}", e);
        }
    }

    private void sendPing() {
        Frame frame = Frame.createFrame(Opcode.PING);

        send(frame);
        pingSent = true;
        pongRec = false;

        // Timer for pong delay, close connection if timer runs out
        Flowable.timer(2, TimeUnit.SECONDS).subscribe(pong -> {
            if(!pongRec) {
                closeConnection(1006);
            }
        });
    }

    private void sendPong(byte[] bytes) {
        Frame frame = Frame.createFrame(Opcode.PONG, bytes);

        send(frame);
        pongSent = true;
    }

    @Override
    public void send(String string) {
        if (isConnected) {
            // FIXME: 26.04.2017 Continuation frames?
            send(Frame.createFrame(Opcode.TEXT_FRAME, string.getBytes()));
        }
    }

    @Override
    public void close() {
        closeConnection(1000);
    }

    public void send(byte[] bytes) {
        send(Frame.createFrame(Opcode.BINARY_FRAME, bytes));
    }

    private void send(Frame frame) {
        try {
            outputStream.write(frame.toBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(Consumer<String> messageAction) {
        clientProcessor.subscribe(messageAction);
    }


    private Action closeAction; // FIXME: 4/27/17
    @Override
    public void onClose(Action action) {

        closeAction = action; // FIXME: 4/27/17
        clientProcessor.doOnComplete(action);
        //clientProcessor.onErrorResumeNext(s -> {}).subscribe(s -> action.run());
    }

    @Override
    public void onError(Consumer<Throwable> action) {
        clientProcessor.doOnError(action);
    }

}
