package server.api;

import io.reactivex.functions.Consumer;
import server.implementation.server.WebSocketServer;

import java.io.IOException;
import java.util.List;

/**
 * Created by OlavH on 4/24/17.
 * Specifies the interface for a Server
 */
public interface Server {

    /**
     * The action to be preformed when someone connects to the server
     * @param connectionAction, the connection of the person connecting
     */
    void onConnection(Consumer<Connection> connectionAction);

    /**
     * The action to be preformed on error
     * @param exceptionAction Consumer with the Exception that occurred
     */
    void onError(Consumer<Exception> exceptionAction);

    /**
     * Closes the server
     */
    void close();

    /**
     * Gets all the Connections currently connected
     * @return list of connections
     */
    List<Connection> getClients();

    /**
     * Starts the server by listening on the specified port, must be called after all on-methods are set.
     */
    void listen();

    /**
     * Sends a message to all currently connected Connections, shorthand for getClients.forEach - send(s)
     * @param string The String to send
     */
    void sendAll(String string);

    /**
     * Gets an implementation object of the Server interface, similar to a factory.
     * @param port The port to listen on.
     * @return Server implementation
     * @throws IOException If anything goes wrong when making the server (port used etc)
     */
    static Server getWebSocketServer(int port) throws IOException {
        return new WebSocketServer(port);
    }
}
