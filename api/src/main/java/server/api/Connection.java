package server.api;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Created by OlavH on 4/24/17.
 * Specifies the interface for a Connection from a client to the server
 */
public interface Connection{

    /**
     * Sends a String to the connection
     * @param string The String to send
     */
    void send(String string);

    /**
     * Closes connection to client. Indicates a normal closure (Status code 1000)
     */
    void close();

    /**
     * The action to be preformed when a message is received by the server from the connection
     * @param messageAction Consumer with the String send by the client
     */
    void onMessage(Consumer<String> messageAction);

    /**
     * The action to be preformed when the connection closes
     * @param action Action to be performed
     */
    void onClose(Action action);

    /**
     * The action to be preformed when an error occurs on the connection
     * @param action Consumer with the Throwable thrown by the connection
     */
    void onError(Consumer<Throwable> action);


    /**
     * Check if connection is active
     * @return true if Connection is active
     */
    boolean isConnected();
}
