package server.api;

/**
 * WebSocket-Project
 * 26.04.2017
 */
public class WebSocketProtocolViolation extends RuntimeException {
    public WebSocketProtocolViolation(String message) {
        super(message);
    }
}
