Frivillig prosjekt
=======
Prosjektoppgaven er frivillig og kan kun utføres av studenter som får C i øvingsdelen av Nettverksprogrammering. Prosjektet gjennomføres av studenter som vil forbedre karakteren i nettverksprogrammeringsdelen til A eller B. Karakteren i nettverksprogrammering teller 50% av totalkarakteren i faget Datakommunikasjon med nettverksprogrammering. Totalkarakteren vil alltid settes til gunst for studenten. En B i datakommunikasjonsdelen sammen med en C i nettverksprogrammering vil altså gi totalkarakteren B.

Merk at alle deler av prosjektet (programmert løsning, dokumentasjon og presentasjon) forventes å holde meget god kvalitet for å oppnå karakteren B eller A.

Maks antall studenter i en gruppe: 3

# Oppgavetekst

Lag et WebSocket server bibliotek i et valgfritt programmeringsspråk. Se presentasjon (forelesning 8) og https://tools.ietf.org/html/rfc6455. Minstekrav:

- bruk av ferdige WebSocket biblioteker er ikke tillatt
- støtte kommunikasjon med 1 klient
    - kommunikasjon med flere klienter er ikke et krav
    - bruk av flere tråder er heller ikke et krav
- skal kunne kommunisere med en nettleser via JavaScript og støtte:
    - handshake
    - små tekstmeldinger begger veier (server til klient og klient til server)
        - større meldinger er ikke et krav
- close
    - status og reason er ikke et krav
- README.md fil for løsningen med eksempel bruk av biblioteket
Bruk av ekstern informasjon skal dokumenteres. Gode implementasjoner utover minstekravet gir positiv uttelling.

Innleveringsfristen er 2. mai klokken 23:59. Send lenke til løsningen til ole.c.eidheim@ntnu.no med tittel "Nettverksprogrammering: innlevering frivillig prosjekt". 

# Presentasjon

Løsningen skal presenteres i løpet av mai. Tidspunkt blir oppgitt senere.

# Sources
https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers